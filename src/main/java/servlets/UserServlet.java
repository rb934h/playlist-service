package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import model.User;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class UserServlet extends HttpServlet {

    UserService userService = new UserService();
    ObjectMapper mapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        if (req.getParameter ("id") != null){
            long id = Long.parseLong(req.getParameter("id"));
            User user = userService.getUser(id);
            PrintWriter out = resp.getWriter();
            out.append(mapper.writeValueAsString(user));
            out.close();
        }else {
            var users = userService.getAll();
            PrintWriter out = resp.getWriter();
            out.append(mapper.writeValueAsString(users));
            out.close();

        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("utf-8");
        PrintWriter out = resp.getWriter();
        var json = req.getReader().lines().collect(Collectors.joining(System.lineSeparator())); // json
        try {
            var model = mapper.readValue(json, User.class);
            Long id = userService.create(model);
            out.append(id.toString());
        } catch (SQLException e) {
            resp.sendError(500);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("utf-8");
        PrintWriter out = resp.getWriter();
        var json = req.getReader().lines().collect(Collectors.joining(System.lineSeparator())); // json
        try {
            var model = mapper.readValue(json, User.class);
            User user = userService.update(model);
            out.append(mapper.writeValueAsString(user));
        } catch (Exception e){
            out.close();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doDelete(req, resp);
    }

}