package model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
public class User {
    private long id;
    private String login;
    private String password;
    private Role role;
}
