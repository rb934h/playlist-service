package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Playlist {
    private long id;
    private Music music;
    private User user;
}
