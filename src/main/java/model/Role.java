package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Role {
    @JsonProperty("Admin")
    Admin,
    @JsonProperty("User")
    User,
    @JsonProperty("Moderator")
    Moderator
}
