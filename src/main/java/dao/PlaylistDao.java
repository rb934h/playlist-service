package dao;

import lombok.extern.slf4j.Slf4j;
import model.Playlist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

@Slf4j
public class PlaylistDao implements Dao<Playlist, Long> {
    static final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/postgres";
    static final String USER = "postgres";
    static final String PASS = "mysecretpassword";
    Connection connection;

    public PlaylistDao() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (Exception e) {
            log.error("Error", e);
        }

    }

    @Override
    public Playlist getById(Long id) {
        try{
            var statement = connection.prepareStatement("SELECT * FROM playlist WHERE id = ?");
        }
        catch (Exception e){log.error("Error", e);}


        return null;
    }

    @Override
    public List<Playlist> getAll() {
        return null;
    }

    @Override
    public Long create(Playlist model) {
        return null;
    }

    @Override
    public Playlist update(Playlist model) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
