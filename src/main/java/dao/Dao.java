package dao;

import java.sql.SQLException;
import java.util.List;

public interface Dao<T, K> {
    T getById(K id);

    List<T> getAll();

    K create(T model) throws SQLException;

    T update(T model);

    void delete(K id);
}
