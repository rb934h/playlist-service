package dao;

import lombok.extern.slf4j.Slf4j;
import model.Role;
import model.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class UserDao implements Dao<User, Long> {
    static final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/postgres"; //
    static final String USER = "postgres";//
    static final String PASS = "mysecretpassword";//
    Connection connection;//

    public UserDao() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (Exception e) {
            log.error("Error", e);
        }

    }

    @Override
    public User getById(Long id) {
        User user = new User();
        try {
            var statement = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
            statement.setLong(1, id);
            var rs = statement.executeQuery();
            while (rs.next()){
                user.setId(rs.getLong("id"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setRole(Role.valueOf(rs.getString("role")));
            }
        } catch (Exception e) {
            log.error("Error", e);
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try {
            var statement = connection.prepareStatement("SELECT id,login,password,role FROM users");
            var rs = statement.executeQuery();
            while (rs.next()){
                User user = new User();
                user.setId(rs.getLong("id"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setRole(Role.valueOf(rs.getString("role")));
                users.add(user);
            }
        } catch (Exception e) {
            log.error("Error", e);
        }
        return users;
    }

    @Override
    public Long create(User model) throws SQLException {
        try {
            var statement = connection.prepareStatement("INSERT INTO users (login,password,role) values (?,?,?) returning id");
            statement.setString(1, model.getLogin());
            statement.setString(2, model.getPassword());
            statement.setString(3, model.getRole().name());
            var rs = statement.executeQuery();
            rs.next();
            return rs.getLong("id");

        } catch (Exception e) {
             log.error("Error", e);
             throw e;
        }

    }

    @Override
    public User update(User model) {
        User user = new User();
        try {
            var statement = connection.prepareStatement("UPDATE users SET  login = ?, password = ?, role = ? WHERE id = ? returning id,login,password,role");
            statement.setString(1, model.getLogin());
            statement.setString(2, model.getPassword());
            statement.setString(3, model.getRole().name());
            statement.setLong(4, model.getId());
            var rs = statement.executeQuery();
            while (rs.next()){
                user.setId(rs.getLong("id"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setRole(Role.valueOf(rs.getString("role")));
            }
        } catch (Exception e) {
            log.error("Error", e);
        }
        return user;
    }


    @Override
    public void delete(Long id) {
        try {
            var statement = connection.prepareStatement("DELETE FROM users WHERE id = ?");
            statement.setLong(1, id);
            var rs = statement.executeQuery();
            rs.deleteRow();
        } catch (Exception e) {
            log.error("Error", e);
        }
    }
}
