package service;

import dao.Dao;
import dao.UserDao;
import model.User;

import java.sql.SQLException;
import java.util.List;

public class UserService {
    Dao<User, Long> dao = new UserDao();
    public User getUser(long id){
        return dao.getById(id);
    }
    public Long create(User model) throws SQLException {
        return dao.create(model);
    }
    public List<User>  getAll(){
        return dao.getAll();
    }
    public void deleteUser(Long id){
         dao.delete(id);
    }
    public User update (User model) {
        return dao.update(model);
    }
}
